# Télécom Nancy MVSI 2018 - _TLA Exercises_

### TLA+

TLA+ is a formal specification language developed by Leslie Lamport. It is used to design, model, document, and verify concurrent systems.<br>
It is also used to write machine-checked proofs of correctness both for algorithms and mathematical theorems.


See more on [Wikipedia](https://en.wikipedia.org/wiki/TLA%2B).


### Tools

[TLA+ Toolbox](https://lamport.azurewebsites.net/tla/toolbox.html)