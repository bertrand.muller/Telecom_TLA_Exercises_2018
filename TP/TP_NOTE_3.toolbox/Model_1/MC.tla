---- MODULE MC ----
EXTENDS fichier, TLC

\* CONSTANT definitions @modelParameterConstants:0max
const_153632564299159000 == 
50
----

\* INIT definition @modelBehaviorInit:0
init_153632564299160000 ==
Init
----
\* NEXT definition @modelBehaviorNext:0
next_153632564299161000 ==
Next3
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_153632564299162000 ==
safety1
----
\* INVARIANT definition @modelCorrectnessInvariants:1
inv_153632564299163000 ==
safety2
----
=============================================================================
\* Modification History
\* Created Fri Sep 07 15:07:22 CEST 2018 by muller337u
