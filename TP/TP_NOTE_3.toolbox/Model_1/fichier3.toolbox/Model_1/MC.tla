---- MODULE MC ----
EXTENDS fichier, TLC

\* CONSTANT definitions @modelParameterConstants:0min
const_1547490181259142000 == 
-999999999
----

\* CONSTANT definitions @modelParameterConstants:2x1
const_1547490181259143000 == 
1
----

\* CONSTANT definitions @modelParameterConstants:3x2
const_1547490181259144000 == 
2
----

\* CONSTANT definitions @modelParameterConstants:4max
const_1547490181259145000 == 
99999999
----

\* INIT definition @modelBehaviorInit:0
init_1547490181259146000 ==
Init
----
\* NEXT definition @modelBehaviorNext:0
next_1547490181259147000 ==
Next
----
\* PROPERTY definition @modelCorrectnessProperties:0
prop_1547490181259148000 ==
Safety1
----
\* PROPERTY definition @modelCorrectnessProperties:1
prop_1547490181259149000 ==
Safety2
----
=============================================================================
\* Modification History
\* Created Mon Jan 14 19:23:01 CET 2019 by muller337u
