--------------- MODULE fichier --------------
(* Modules de base importables *)
EXTENDS Naturals, Integers, TLC
---------------------------------------------
CONSTANTS min, max, x1, x2
---------------------------------------------

(*
--algorithm ex3 {
    variables y1;
              y2;
              y3;
              z;
    {
        l0: print<<x1,x2>>;
        
        y1:=x1;
        y2:=x2;
        y3:=1;
        
        l1:while(y2 /= 0) {
            
            l2: if(y2%2 /= 0) {
            
                l3: y2:=y2-1;
                l4: y3:=y3*y1;
                l5: skip;
            
            };
            
            l6: y1:=y1*y1;
            l7: y2:=y2 \div 2;
            l8: skip;
            
        };
        
        l9: z:=y3;
        l10: print<<x1,x2,z>>;
        
    }
}
*)
\* BEGIN TRANSLATION
CONSTANT defaultInitValue
VARIABLES y1, y2, y3, z, pc

vars == << y1, y2, y3, z, pc >>

Init == (* Global variables *)
        /\ y1 = defaultInitValue
        /\ y2 = defaultInitValue
        /\ y3 = defaultInitValue
        /\ z = defaultInitValue
        /\ pc = "l0"

l0 == /\ pc = "l0"
      /\ PrintT(<<x1,x2>>)
      /\ y1' = x1
      /\ y2' = x2
      /\ y3' = 1
      /\ pc' = "l1"
      /\ z' = z

l1 == /\ pc = "l1"
      /\ IF y2 /= 0
            THEN /\ pc' = "l2"
            ELSE /\ pc' = "l9"
      /\ UNCHANGED << y1, y2, y3, z >>

l2 == /\ pc = "l2"
      /\ IF y2%2 /= 0
            THEN /\ pc' = "l3"
            ELSE /\ pc' = "l6"
      /\ UNCHANGED << y1, y2, y3, z >>

l3 == /\ pc = "l3"
      /\ y2' = y2-1
      /\ pc' = "l4"
      /\ UNCHANGED << y1, y3, z >>

l4 == /\ pc = "l4"
      /\ y3' = y3*y1
      /\ pc' = "l5"
      /\ UNCHANGED << y1, y2, z >>

l5 == /\ pc = "l5"
      /\ TRUE
      /\ pc' = "l6"
      /\ UNCHANGED << y1, y2, y3, z >>

l6 == /\ pc = "l6"
      /\ y1' = y1*y1
      /\ pc' = "l7"
      /\ UNCHANGED << y2, y3, z >>

l7 == /\ pc = "l7"
      /\ y2' = (y2 \div 2)
      /\ pc' = "l8"
      /\ UNCHANGED << y1, y3, z >>

l8 == /\ pc = "l8"
      /\ TRUE
      /\ pc' = "l1"
      /\ UNCHANGED << y1, y2, y3, z >>

l9 == /\ pc = "l9"
      /\ z' = y3
      /\ pc' = "l10"
      /\ UNCHANGED << y1, y2, y3 >>

l10 == /\ pc = "l10"
       /\ PrintT(<<x1,x2,z>>)
       /\ pc' = "Done"
       /\ UNCHANGED << y1, y2, y3, z >>

Next == l0 \/ l1 \/ l2 \/ l3 \/ l4 \/ l5 \/ l6 \/ l7 \/ l8 \/ l9 \/ l10
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

BF(X) == X \in min..max
Safety1 == BF(y1) /\ BF(y2) /\ BF(y3) /\ BF(z)
Safety2 == pc \in {"Done"} => z=x1^x2

\* END TRANSLATION

=============================================


