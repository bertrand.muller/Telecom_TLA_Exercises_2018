---- MODULE MC ----
EXTENDS fichier, TLC

\* CONSTANT definitions @modelParameterConstants:0min
const_154748918611189000 == 
-1000
----

\* CONSTANT definitions @modelParameterConstants:1max
const_154748918611190000 == 
1000
----

\* INIT definition @modelBehaviorInit:0
init_154748918611191000 ==
Init
----
\* NEXT definition @modelBehaviorNext:0
next_154748918611192000 ==
Next
----
\* INVARIANT definition @modelCorrectnessInvariants:0
inv_154748918611193000 ==
Safety
----
=============================================================================
\* Modification History
\* Created Mon Jan 14 19:06:26 CET 2019 by muller337u
