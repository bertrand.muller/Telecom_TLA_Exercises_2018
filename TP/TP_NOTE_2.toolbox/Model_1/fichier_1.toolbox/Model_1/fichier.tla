--------------- MODULE fichier --------------
(* Modules de base importables *)
EXTENDS Naturals, Integers, TLC
---------------------------------------------
CONSTANTS min, max
---------------------------------------------

(*
--algorithm ex2 {
    variables c = 5;
              x = 81;
              z = 2*c;
              y = (z+1)*(z+1);
    {
        l1: y:=x+z+1;
        l2: skip;
    }
}
*)
\* BEGIN TRANSLATION
VARIABLES c, x, z, y, pc

vars == << c, x, z, y, pc >>

Init == (* Global variables *)
        /\ c = 5
        /\ x = 81
        /\ z = 2*c
        /\ y = (z+1)*(z+1)
        /\ pc = "l1"

l1 == /\ pc = "l1"
      /\ y' = x+z+1
      /\ pc' = "l2"
      /\ UNCHANGED << c, x, z >>

l2 == /\ pc = "l2"
      /\ TRUE
      /\ pc' = "Done"
      /\ UNCHANGED << c, x, z, y >>

Next == l1 \/ l2
           \/ (* Disjunct to prevent deadlock on termination *)
              (pc = "Done" /\ UNCHANGED vars)

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

Safety == pc = "l1" => x = 81 /\ z = 2*c /\ y = (z+1)*(z+1)

\* END TRANSLATION

=============================================


